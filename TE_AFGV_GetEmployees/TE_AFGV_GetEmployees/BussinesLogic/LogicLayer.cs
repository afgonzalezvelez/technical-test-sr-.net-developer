﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using TE_AFGV_GetEmployees.Models;
using static TE_AFGV_GetEmployees.Models.DAL_GetEmployeesFrom_API;

namespace TE_AFGV_GetEmployees.BussinesLogic
{
    public class LogicLayer
    {

        public struct ResponseEmployeeDOT
        {
            public int ResultCode;
            public string ResultMessage;
            public string id;
            public string Name;
            public string ContractType;
            public int RoleIdentifier;
            public string Role;
            public string RoleDescription;
            public double salary;
            
        }
        //DTO Employee with Hourly Salary
        public struct ResponseHourlySalaryEmployeeDOT
        {
           public double AnnualSalaryHourly;

        }

        //DTO Employee with Monthly Salary
        public struct ResponseMonthlySalaryEmployeeDOT
        {
           
            public double AnnualSalaryMonthly;

        }

        ResponseFromAPI ResponseApi = new ResponseFromAPI();

        public List<ResponseEmployeeDOT> GetEmployeesInfo(int id)
        {
            // LOGIC LAYER
            /* Codes translation:
             *  1: OK.
             *  2: No employees from the API.
             * -1: Some error from the DAL capturing employees from the API.
             * -2: Exception on any part of the code.
             */

            #region Variables
            JObject response = new JObject();
            JObject employee = new JObject();
            response["response_ws"] = "NOK";
            response["result_code"] = "NOK";
            response["excepcion"] = "NOK";
            DAL_GetEmployeesFrom_API api = new DAL_GetEmployeesFrom_API();

            List<ResponseEmployeeDOT> objEmployeeArray = new List<ResponseEmployeeDOT>();
            ResponseEmployeeDOT objEmployee = new ResponseEmployeeDOT();
            ResponseMonthlySalaryEmployeeDOT objEmployeeMonthly = new ResponseMonthlySalaryEmployeeDOT();
            ResponseHourlySalaryEmployeeDOT objEmployeeHourly = new ResponseHourlySalaryEmployeeDOT();
            JArray responseEmployee = new JArray();
            double salary = 0;
            #endregion
            try
            {

                #region LogicLayer
                //Returning from DAL the response of the API
                ResponseApi = api.GetEmployeesFromAPI();
                if (ResponseApi.ResponseCode == 1)
                {
                    responseEmployee = ResponseApi.Employees;
                    foreach (JObject item in responseEmployee)
                    {
                        //Validate if the array has employees.
                        if (item.HasValues)
                        {
                            objEmployee.ResultCode = 1;
                            objEmployee.ResultMessage = "OK";
                            objEmployee.id = item.GetValue("id").ToString();
                            objEmployee.Name = item.GetValue("name").ToString();
                            objEmployee.ContractType = item.GetValue("contractTypeName").ToString();
                            objEmployee.RoleIdentifier = Convert.ToInt32(item.GetValue("roleId").ToString());
                            objEmployee.Role = item.GetValue("roleName").ToString();
                            objEmployee.RoleDescription = item.GetValue("roleDescription").ToString();

                            //calculate Annual Salary
                            switch (objEmployee.ContractType)
                            {
                                //if contract type is "HourlySalaryEmployee"
                                case "HourlySalaryEmployee":
                                    objEmployeeHourly.AnnualSalaryHourly = CalculateAnnualSalaryHourly(Convert.ToInt32(item.GetValue("hourlySalary").ToString()));
                                    objEmployee.salary = objEmployeeHourly.AnnualSalaryHourly;
                                    break;
                                //if contract type is "MonthlySalaryEmployee"
                                case "MonthlySalaryEmployee":
                                    objEmployeeMonthly.AnnualSalaryMonthly = CalculateAnnualSalaryMonthly(Convert.ToInt32(item.GetValue("hourlySalary").ToString()));
                                    objEmployee.salary = objEmployeeMonthly.AnnualSalaryMonthly;
                                    break;
                                default:
                                    break;

                            }

                        }
                        else
                        {
                            objEmployee.ResultCode = 2;
                        }
                        //validate if an id come in the request
                        //If is 0 it means that its not searching for an employee so we add it to the array
                        if (id == 0)
                        {
                            objEmployeeArray.Add(objEmployee);
                        }
                        else
                        {   
                            if (Convert.ToInt32(objEmployee.id) == id)
                            {
                                objEmployeeArray.Add(objEmployee);
                            }
                        }
                        

                    }

                }
                else
                {
                    objEmployee.ResultCode = -1;
                    objEmployee.ResultMessage = ResponseApi.Message;
                }

                
                #endregion
                return objEmployeeArray;
            }
            catch (Exception ex)
            {

                objEmployee.ResultCode = -2;
                objEmployee.ResultMessage = ex.Message;

               
                return objEmployeeArray;

            }

        }

        public double CalculateAnnualSalaryHourly(int hourlySalary)
        {
            //calculating Annual Salary
            double result = 0;
            result = 120 * hourlySalary * 12;
            return result;
        }

        public double CalculateAnnualSalaryMonthly(int monthlySalary)
        {
            //calculating Annual Salary
            double result = 0;
            result = monthlySalary * 12;
            return result;
        }
    }
}