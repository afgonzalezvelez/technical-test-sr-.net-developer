﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace TE_AFGV_GetEmployees.Models
{
    public class DAL_GetEmployeesFrom_API
    {
        //DTO for response
        public struct ResponseFromAPI
        {
            public int ResponseCode;
            public string Message;
            public JArray Employees;

        }
         
        public ResponseFromAPI GetEmployeesFromAPI()
        {
            #region Variables
            /*ResponseCode translation:
                 1: OK
                 2: Not OK, return HTTP status code
                -1: Error code
             */
            ResponseFromAPI response = new ResponseFromAPI();
            //Response code for default
            response.ResponseCode = -1;
            JArray ResponseWhitoutInfo = new JArray();
            #endregion
            #region Logic
            try
            {
                //Request to the API
                string URL = System.Configuration.ConfigurationManager.AppSettings["ApiServiceURL"].ToString();
                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(URL);

                httpRequest.Method = "GET";
                httpRequest.ContentType = "application/json;";
                httpRequest.Accept = "application/json";
                HttpWebResponse resp = (HttpWebResponse)httpRequest.GetResponse();

                string respStr = "";
                //Validating if we have any type of response
                if (httpRequest.HaveResponse)
                {
                    //having a response code 200 (OK) 
                    if (resp.StatusCode == HttpStatusCode.OK)
                    {
                        StreamReader respReader = new StreamReader(resp.GetResponseStream());
                        respStr = respReader.ReadToEnd(); // get the json result in the string object  
                        response.ResponseCode = 1;
                        response.Message = "OK";
                        response.Employees = JArray.Parse(respStr);

                    }
                    else
                    {
                        //Reponse not OK
                        response.ResponseCode = 2;
                        response.Message = "Status code: "+ resp.StatusCode;
                        response.Employees = ResponseWhitoutInfo;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                //Error treatment
                response.ResponseCode = -1;
                response.Message = "Error: " + ex.Message;
                response.Employees = ResponseWhitoutInfo;
                return response;

            }
            #endregion
        }


    }
}