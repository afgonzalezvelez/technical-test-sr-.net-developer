﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static TE_AFGV_GetEmployees.BussinesLogic.LogicLayer;

namespace TE_AFGV_GetEmployees.Controllers
{
    public class GetEmployeesController : ApiController
    {

        [Route("api/getEmployeesAFGV/{id?}")]
        [AcceptVerbs("GET")]
        [HttpGet]
        public IHttpActionResult getEmployee(int id = 0)
        {
            List<ResponseEmployeeDOT> response = new List<ResponseEmployeeDOT>();
            BussinesLogic.LogicLayer logicObject = new BussinesLogic.LogicLayer();
            JObject res = new JObject();
            res["response_ws"] = "NOK";
            res["result_code"] = "NOK";
            res["exception"] = "NOK";

            response = logicObject.GetEmployeesInfo(id);
            if (response.Count > 0)
            {

                var jsonResponse = JsonConvert.SerializeObject(response);
                res["result_code"] = "OK";
                res["exception"] = "NOK";
                res["response_ws"] = jsonResponse;

            }

            return Content(HttpStatusCode.OK, res);

        }

       
    }
}
