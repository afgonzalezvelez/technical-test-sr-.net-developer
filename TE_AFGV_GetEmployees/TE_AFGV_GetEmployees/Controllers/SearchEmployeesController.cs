﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TE_AFGV_GetEmployees.Controllers
{
    public class SearchEmployeesController : Controller
    {
        // GET: SearchEmployees
        public ActionResult Index()
        {


            return View();
        }

        // GET: SearchEmployees/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SearchEmployees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SearchEmployees/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SearchEmployees/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SearchEmployees/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SearchEmployees/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SearchEmployees/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
