﻿using System.Web;
using System.Web.Mvc;

namespace TE_AFGV_GetEmployees
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
