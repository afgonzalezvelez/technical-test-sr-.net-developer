﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TE_AFGV_GetEmployees.Tests.Controllers
{
    [TestClass]
    public class testCalcAnnualSalary
    {
        [TestMethod]
        public void Test_CalculateAnnualSalaryHourly()
        {
            // Arrange
            BussinesLogic.LogicLayer layer = new BussinesLogic.LogicLayer();
            int hour = 1;
          
            // Act
            double result = layer.CalculateAnnualSalaryHourly(hour);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1440, result);
          
        }

        [TestMethod]
        public void Test_CalculateAnnualSalaryMonthly()
        {
            // Arrange
            BussinesLogic.LogicLayer layer = new BussinesLogic.LogicLayer();
            int hour = 1;

            // Act
            double result = layer.CalculateAnnualSalaryMonthly(hour);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(12, result);

        }
    }
}
